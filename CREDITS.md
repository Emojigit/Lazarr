CREDITS for Lazarr!

Lazarr! is a puzzle game by Wuzzy, using some textures, sounds and code from others.

External mods used:

* `show_wielded_item`
* `player_api`, `stairs`, `screwdriver2`

---

Textures:
- Water, wood, tree, stone, tools, hand, gold block, wooden chest, locked wooden chest, ship lightbox, player textures, crates, seaweed, purple coral, crab grass, fire come from “PixelBOX” texture pack by jp (CC0)
- Textures in `lzr_decor` mod made by jp (CC0)
- Barricade, dark chest made by jp (CC0)
- Crosshair, wieldhand, `smoke_puff.png`: trivial textures by Wuzzy (CC0)
- Emitter, detector, dark locked chest textures: Derivate works of xdecor textures by jp (CC0)
- Mirror textures: by Wuzzy (CC0)
- Screwdriver texture: by 12Me21 (CC0)
- Hotbar textures are a recolored version from from the hotbar textures of the Isabella Texture Pack by Bonemouse (http://www.minecraftforum.net/topic/242175-Isabella/) (CC BY 3.0)
- `default_dirt.png`, `default_grass.png`, `default_grass_side.png`, `default_rainforest_litter.png`, `default_rainforest_litter_side.png`, `default_sand.png`, `lzr_core_seabed.png`, `lzr_core_shrub_leaves.png`, `lzr_core_palm_leaves.png`. `lzr_core_palm_leaves_top.png`, `lzr_core_palm_tree.png`, `lzr_core_palm_tree_top.png`, `islands_tall_grass.png`: by TheThermos (MIT)
- `lzr_gui_bg.png`: Based on two works:
	- 1) <https://opengameart.org/content/sheet-of-old-paper> by Q\_x (CC0)
	- 2) <https://opengameart.org/content/rpg-gui-construction-kit-v10> by Lamoot (CC BY 3.0)
- `lzr_gui_button.png`, `lzr_gui_button_hover.png`, `lzr_gui_button_pressed.png`:
	- Based on <https://opengameart.org/content/rpg-gui-construction-kit-v10> by Lamoot (CC BY 3.0)
- Hook texture: CraftPix.net 2D Game Assets
	- Derivate work of <https://opengameart.org/content/48-pirate-stuff-icons>
	- License: OGA-BY 3.0 <https://static.opengameart.org/OGA-BY-3.0.txt>
- `xocean_*.png`:
	- By StarNinjas
	- License: MIT License
- `lzr_decor_ocean_pillar.png`:
	- By StarNinjas
	- License: MIT License
	- Original file name `xocean_pillar.png`
- `lzr_decor_ocean_pillar_top.png`:
	- By StarNinjas and Wuzzy
	- Derivate work of `xocean_pillar.png`
	- License: MIT License
- `lzr_decor_ocean_stone_block.png`:
	- By StarNinjas and Wuzzy
	- Derivate work of `xocean_stone.png`
	- License: MIT License

Models:
- Player models comes from Minetest Game (see license of Minetest Game 5.4.1 for details)

Sounds:
- `lzr_ambience_ocean.ogg` comes from <https://freesound.org/people/inchadney/sounds/135805/>
	- by inchadney (CC BY 3.0)
- `lzr_ambience_temple.ogg` comes from <https://freesound.org/people/Zixem/sounds/69391/>
	- by Zixem (CC0)
- Mirror rotation sound comes from <https://freesound.org/people/killianm97/sounds/554236/>
	- by killanm97 (CC0)
- `screwdriver2_rotate.ogg` comes from <https://freesound.org/people/el_boss/sounds/560700/>
	- by `el_boss` (CC0)
- Glass sounds:
	- Footstep: Derivate work from <https://freesound.org/people/deleted_user_2104797/sounds/325252/>
		- by `deleted_user_2104797` (CC0)
	- Dig: Derivate work from <https://freesound.org/people/kelsey_w/sounds/467039/>
		- by `kelsey_w` (CC BY 3.0)
	- Place: Derivate work of <https://freesound.org/people/kbnevel/sounds/119839/>
		- by kbnevel (CC0)
- Wood sounds:
	- Footstep:
		- by Jan Schupke 'Vehicle' (http://www.vehiclemusic.eu jan.schupke@gmail.com)
		- License: CC0
		- Origin:
			- <http://opengameart.org/content/fantasy-sound-effects-tinysized-sfx>
        		- <http://opengameart.org/content/fantasy-accessory-sfx-library>
        		- <http://opengameart.org/content/fantasy-weapons-and-apparel-sfx-library>
		- All sounds are organic and unprocessed, apart from normalization and downmixing to mono.
		- 96 files, 44.1kHz, 16bit.
	- Place/dug:
		- by youandbiscuitme
		- License: CC BY 3.0
		- Origin: <https://freesound.org/people/youandbiscuitme/sounds/258244/>
		- Changes were made
- `lzr_laser_emitter_activate.ogg`: 
	- by PhonosUPF <https://freesound.org/people/PhonosUPF/sounds/501976/>
	- License: CC0
- `lzr_levels_level_start.ogg`: 
	- by original\_sound <https://freesound.org/people/original_sound/sounds/372209/>
	- License: CC BY 3.0
- `lzr_levels_level_complete.ogg`:
	- by Fupicat <https://freesound.org/people/Fupicat/sounds/521641/>
	- Edit by Wuzzy (higher pitch)
	- License: CC0
- `lzr_sounds_button.ogg`:
	- by Fourier <https://opengameart.org/content/forward-button-press-ui-sound>
	- Edit by Wuzzy (shortened)
	- License: CC BY 3.0
- `lzr_laser_quickburn.*.ogg`:
	- Original by florianreichelt <https://freesound.org/people/florianreichelt/sounds/563012/>
	- Shortened for gameplay reasons
	- License: CC0
- `lzr_laser_barricade_break.ogg`:
	- By kevinkace <https://freesound.org/people/kevinkace/sounds/66780/>
	- License: CC0
- `lzr_sounds_footstep_stone.*.ogg`, `default_dirt_footstep.*.ogg`, `lzr_sounds_footstep_leaves.*.ogg`, `lzr_sounds_dug_grass.*.ogg`:
	- by Wuzzy
	- License: MIT License
- `lzr_treasure_chest_open.ogg`:
	- by JUDITH136 <https://freesound.org/people/JUDITH136/sounds/408001/>
	- License: CC BY 3.0
- `lzr_treasure_chest_open_fail.ogg`:
	- by Dymewiz <https://freesound.org/people/Dymewiz/sounds/131029/>
	- License: CC BY 3.0
- `lzr_treasure_chest_lock_break.ogg`:
	- by MAJ061785 <https://freesound.org/people/MAJ061785/sounds/85533/>
	- A shortened version of the original sound is used for this game
	- License: CC BY 3.0

- All other sounds come from Minetest Game (see license of Minetest Game 5.4.1 for details)

Code:
- `player_api`, `stairs` come from Minetest Game 5.4.1 (mods modified for Lazarr!), by Minetest Game developers (see README.txt in those folders)
- `screwdriver2` by 12Me21, modified version (CC0)
- `lzr_panes` is based on `xpanes` from Minetest Game and massively simplified (MIT License for code, CC BY-SA 3.0 for media)
- `lzr_decor` is based on `xdecor` by jp (BSD-3 clause license; massively-simplified version from the original)

Other stuff:
- See the licence files of the individual mods

Code for all the mods that begin with "lzr_" (e.g. the mod "`lzr_laser`")
- done by Wuzzy (GPLv3+)

`no_fall_damage`, `no_multiplayer`, `show_wielded_item` mods
- done by Wuzzy (CC0)

