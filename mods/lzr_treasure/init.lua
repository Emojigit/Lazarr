local S = minetest.get_translator("lzr_treasure")
local N = function(s) return s end

-- Register treasure blocks --
minetest.register_node("lzr_treasure:gold_block", {
	description = S("Gold Block"),
	tiles = {"lzr_treasure_gold_block.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_metal_defaults(),
})

-- Register chests --

local register_chest = function(id, def)
	local treasure_id = "gold"
	local sound_open = def.sound_open or "lzr_treasure_chest_open"
	local sound_open_fail = def.sound_open_fail or "lzr_treasure_chest_open_fail"
	local sound_lock_break = def.sound_lock_break or "lzr_treasure_chest_lock_break"

	minetest.register_node("lzr_treasure:chest_"..id.."_unlocked", {
		description = def.description_unlocked,
		paramtype2 = "facedir",
		tiles = { def.tile_top, def.tile_bottom, def.tile_side, def.tile_side, def.tile_side, def.tile_front },
		groups = { breakable = 1, chest = 1, chest_closed = 1, rotatable = 3 },
		sounds = def.node_sounds,
		on_rotate = screwdriver.rotate_simple,
		on_punch = function(pos, node, puncher)
			if lzr_gamestate.get_state() ~= lzr_gamestate.LEVEL then
				return
			end
			minetest.log("action", "[lzr_treasure] "..puncher:get_player_name().." opens "..node.name.." at "..minetest.pos_to_string(pos))
			minetest.set_node(pos, {name="lzr_treasure:chest_"..id.."_open_"..treasure_id, param2=node.param2})
			minetest.sound_play({name=sound_open, gain=0.5}, {pos=pos}, true)
			lzr_levels.found_treasure()
			local timer = minetest.get_node_timer(pos)
			timer:start(1)
		end,
	})

	minetest.register_node("lzr_treasure:chest_"..id.."_locked", {
		description = def.description_locked,
		paramtype2 = "facedir",
		tiles = { def.tile_top, def.tile_bottom, def.tile_side, def.tile_side, def.tile_side, def.tile_front_lock },
		groups = { breakable = 1, chest = 2, chest_closed = 1, rotatable = 3 },
		sounds = def.node_sounds,
		on_rotate = screwdriver.rotate_simple,
		on_punch = function(pos, node, puncher)
			if lzr_gamestate.get_state() ~= lzr_gamestate.LEVEL then
				return
			end
			minetest.sound_play({name=sound_open_fail, gain=0.3}, {pos=pos}, true)
		end,
		-- Unlock chest
		_lzr_unlock = function(pos, node)
			minetest.set_node(pos, {name="lzr_treasure:chest_"..id.."_unlocked", param2=node.param2})

			minetest.sound_play({name=sound_lock_break, gain=0.8}, {pos=pos}, true)

			local dir = minetest.facedir_to_dir(node.param2)
			local w = 3/16
			local k = 9/16
			local l = 8/16
			local minoff, maxoff
			if dir.x > 0 then
				minoff = vector.new(-k, -w, -w)
				maxoff = vector.new(-l, w, w)
			elseif dir.x < 0 then
				minoff = vector.new(l, -w, -w)
				maxoff = vector.new(k, w, w)
			elseif dir.z > 0 then
				minoff = vector.new(-w, -w, -k)
				maxoff = vector.new(w, w, -l)
			elseif dir.z < 0 then
				minoff = vector.new(-w, -w, l)
				maxoff = vector.new(w, w, k)
			end
			if minoff then
				minetest.add_particlespawner({
					amount = 12,
					time = 0.001,
					minpos = vector.add(pos, minoff),
					maxpos = vector.add(pos, maxoff),
					minvel = vector.new(-0.5, -0.2, -0.5),
					maxvel = vector.new(0.5, 0.5, 0.5),
					minacc = vector.new(0, -lzr_globals.GRAVITY, 0),
					maxacc = vector.new(0, -lzr_globals.GRAVITY, 0),
					minsize = 0.5,
					maxsize = 0.5,
					minexptime = 0.60,
					maxexptime = 0.65,
					texture = "lzr_treasure_particle_lock.png",
				})
			end
		end,
	})

	minetest.register_node("lzr_treasure:chest_"..id.."_open", {
		description = def.description_open,
		drawtype = "nodebox",
		paramtype = "light",
		collision_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, 2/16, 0.5 },
		},
		selection_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, 2/16, 0.5 },
		},
		node_box = {
			type = "fixed",
			fixed = {
				{ -0.5, -0.5, -0.5, -7/16, 2/16, 0.5 },
				{ 7/16, -0.5, -0.5, 0.5, 2/16, 0.5 },
				{ -7/16, -0.5, -0.5, 7/16, 2/16, -7/16 },
				{ -7/16, -0.5, 7/16, 7/16, 2/16, 0.5 },
				{ -7/16, -0.5, -7/16, 7/16, -7/16, 7/16 },
			},
		},
		tiles = { def.tile_top, def.tile_bottom, def.tile_side, def.tile_side, def.tile_side, def.tile_front },
		paramtype2 = "facedir",
		groups = { breakable = 1, chest = 3, chest_open = 1, rotatable = 3 },
		sounds = def.node_sounds,
		on_rotate = screwdriver.rotate_simple,
	})

	-- Open chest with treasure

	local treasure = "lzr_treasure:gold_block"
	local treasure_tile_top = "lzr_treasure_gold_block_in_chest_top.png"
	local treasure_tile_side = "lzr_treasure_gold_block_in_chest_side.png"

	local treasure_def = minetest.registered_nodes[treasure]
	local sounds_open_treasure = table.copy(def.node_sounds)
	sounds_open_treasure.footstep = table.copy(treasure_def.sounds.footstep)
	sounds_open_treasure.dig = nil
	sounds_open_treasure.dug = table.copy(treasure_def.sounds.dug)

	minetest.register_node("lzr_treasure:chest_"..id.."_open_"..treasure_id, {
		description = S(def.description_open_with, treasure_def.description),
		drawtype = "nodebox",
		paramtype = "light",
		node_box = {
			type = "fixed",
			fixed = {
				{ -0.5, -0.5, -0.5, 0.5, 2/16, 0.5 },
				{ -7/16, 2/16, -7/16, 7/16, 0.5, 7/16 },
			},
		},
		tiles = {
			"("..def.tile_top..")^("..treasure_tile_top.."^[mask:lzr_treasure_mask_block_in_chest_top.png)",
			def.tile_bottom,
			"("..def.tile_side..")^("..treasure_tile_side.."^[mask:lzr_treasure_mask_block_in_chest_side.png)",
			"("..def.tile_side..")^("..treasure_tile_side.."^[mask:lzr_treasure_mask_block_in_chest_side.png)",
			"("..def.tile_side..")^("..treasure_tile_side.."^[mask:lzr_treasure_mask_block_in_chest_side.png)",
			"("..def.tile_front..")^("..treasure_tile_side.."^[mask:lzr_treasure_mask_block_in_chest_side.png)",
		},
		on_timer = function(pos)
			local done = lzr_laser.check_level_won()
			if done then
				lzr_levels.level_complete()
			end
		end,
		-- Removes the treasure from the chest and spawns particles (for level finish)
		_lzr_send_treasure = function(pos, node)
			for i=0,30 do
				minetest.after(i*0.0625, function(it)
					local amount = 10
					if it > 20 then
						amount = 31 - it
					end
					minetest.add_particlespawner({
						amount = amount,
						time = 0.001,
						minpos = vector.add(pos, vector.new(-7/16, -4/16+it*0.5, -7/16)),
						maxpos = vector.add(pos, vector.new(7/16, 4/16+it*0.5, 7/16)),
						minvel = vector.new(-0.2, -0.2, -0.2),
						maxvel = vector.new(0.2, 0.2, 0.2),
						minsize = 0.5,
						maxsize = 0.5,
						node = {name=treasure},
					})
				end, i)
			end
			minetest.set_node(pos, {name="lzr_treasure:chest_"..id.."_open", param2=node.param2})
		end,
		paramtype2 = "facedir",
		groups = { breakable = 1, chest = 4, chest_open = 1, chest_open_treasure = 1, rotatable = 3, },
		sounds = sounds_open_treasure,
		on_rotate = screwdriver.rotate_simple,
	})

end

register_chest("wood", {
	description_unlocked = S("Wooden Chest"),
	description_locked = S("Locked Wooden Chest"),
	description_open = S("Open Wooden Chest"),
	description_open_with = N("Open Wooden Chest with @1"),
	tile_top = "lzr_treasure_chest_top.png",
	tile_bottom = "lzr_treasure_chest_top.png",
	tile_side = "lzr_treasure_chest_side.png",
	tile_front = "lzr_treasure_chest_front.png",
	tile_front_lock = "lzr_treasure_chest_lock.png",
	node_sounds = lzr_sounds.node_sound_wood_defaults(),
})

register_chest("dark", {
	description_unlocked = S("Dark Chest"),
	description_locked = S("Locked Dark Chest"),
	description_open = S("Open Dark Chest"),
	description_open_with = N("Open Dark Chest with @1"),
	tile_top = "lzr_treasure_dark_chest_top.png",
	tile_bottom = "lzr_treasure_dark_chest_top.png",
	tile_side = "lzr_treasure_dark_chest_side.png",
	tile_front = "lzr_treasure_dark_chest_front.png",
	tile_front_lock = "lzr_treasure_dark_chest_lock.png",
	node_sounds = lzr_sounds.node_sound_stone_defaults(),
})
