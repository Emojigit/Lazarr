local S = minetest.get_translator("lzr_core")

minetest.register_node("lzr_core:stone", {
	description = S("Stone"),
	tiles = {"default_stone.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:seabed", {
	description = S("Seabed"),
	tiles = {"lzr_core_seabed.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_sand_defaults(),
})

minetest.register_node("lzr_core:sand", {
	description = S("Sand"),
	tiles = {"default_sand.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_sand_defaults(),
})

minetest.register_node("lzr_core:dirt", {
	description = S("Dirt"),
	tiles = {"default_dirt.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults(),
})

minetest.register_node("lzr_core:dirt_with_grass", {
	description = S("Dirt with Grass"),
	tiles = {"default_grass.png", "default_dirt.png",
		{name = "default_dirt.png^default_grass_side.png", tileable_vertical = false}},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults(),
})

minetest.register_node("lzr_core:dirt_with_jungle_litter", {
	description = S("Dirt with Jungle Litter"),
	tiles = {"default_rainforest_litter.png", "default_dirt.png",
		{name = "default_dirt.png^default_rainforest_litter_side.png", tileable_vertical = false}},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults(),
})

minetest.register_node("lzr_core:shrub_leaves", {
	description = S("Shrub Leaves"),
	paramtype = "light",
	drawtype = "allfaces",
	tiles = {"lzr_core_shrub_leaves.png"},
	use_texture_alpha = "clip",
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_leaves_defaults(),
	is_ground_content = false,
})

local lb = 0.499 --leaves border
minetest.register_node("lzr_core:bright_palm_leaves", {
	description = S("Bright Palm Leaves"),
	paramtype = "light",
	drawtype = "nodebox",
	collision_box = {
		type = "fixed",
		fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
	},
	node_box = {
		type = "fixed",
		fixed = { -lb, -0.5, -lb, lb, lb, lb },
	},
	tiles = {
		{name="lzr_core_palm_leaves_top.png",backface_culling = false},
		{name="blank.png"},
		{name="lzr_core_palm_leaves.png",backface_culling = false},
	},
	use_texture_alpha = "clip",
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_leaves_defaults(),
	is_ground_content = false,
})

minetest.register_node("lzr_core:dark_palm_leaves", {
	description = S("Dark Palm Leaves"),
	paramtype = "light",
	drawtype = "nodebox",
	collision_box = {
		type = "fixed",
		fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
	},
	node_box = {
		type = "fixed",
		fixed = { -lb, -0.5, -lb, lb, lb, lb },
	},
	tiles = {
		{name="lzr_core_palm_leaves_top.png",backface_culling = false},
		{name="blank.png"},
		{name="lzr_core_palm_leaves.png",backface_culling = false},
	},
	color = "#00B000",
	use_texture_alpha = "clip",
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_leaves_defaults(),
	is_ground_content = false,
})

minetest.register_node("lzr_core:tree", {
	description = S("Tree Trunk"),
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = { breakable = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("lzr_core:palm_tree", {
	description = S("Palm Tree Trunk"),
	tiles = {"lzr_core_palm_tree_top.png", "lzr_core_palm_tree_top.png", "lzr_core_palm_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = { breakable = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("lzr_core:wood", {
	description = S("Wood Planks"),
	tiles = {"default_wood.png"},
	is_ground_content = false,
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})



minetest.register_node("lzr_core:water_source", {
	description = S("Water Source"),
	drawtype = "liquid",
	waving = 3,
	tiles = {
		{
			name = "default_water_source_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
		{
			name = "default_water_source_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	use_texture_alpha = "blend",
	paramtype = "light",
	walkable = false,
	buildable_to = false,
	is_ground_content = false,
	liquidtype = "source",
	liquid_alternative_flowing = "lzr_core:water_source",
	liquid_alternative_source = "lzr_core:water_source",
	liquid_viscosity = 1,
	liquid_range = 0,
	liquid_renewable = false,
	liquids_pointable = true,
	post_effect_color = {a = 192, r = 17, g = 69, b = 135},
	groups = {water = 3, liquid = 3},
})

-- For invisible level borders
minetest.register_node("lzr_core:barrier", {
	description = S("Barrier"),
	drawtype = "airlike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	wield_image = "lzr_core_barrier.png",
	inventory_image = "lzr_core_barrier.png",
	groups = { breakable = 1, not_in_creative_inventory = 1 },

	-- Prevent building things on the barrier
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		return itemstack
	end,
})

