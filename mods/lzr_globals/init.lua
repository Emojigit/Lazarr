lzr_globals = {}

lzr_globals.LEVEL_POS = vector.new(0, 0, 0)
lzr_globals.PLAYFIELD_START = vector.add(vector.new(-1, -1, -1), lzr_globals.LEVEL_POS)
lzr_globals.PLAYFIELD_SIZE = vector.new(22, 22, 22)
lzr_globals.PLAYFIELD_END = vector.add(lzr_globals.PLAYFIELD_START, lzr_globals.PLAYFIELD_SIZE)
lzr_globals.DEFAULT_LEVEL_SIZE = vector.new(10, 6, 10)
lzr_globals.MENU_SHIP_POS = vector.new(-500, -4, -500)
lzr_globals.MENU_SHIP_PLAYER_SPAWN_OFFSET = vector.new(7, 8.5, 29)
lzr_globals.MENU_SHIP_STARTBOOK_OFFSET = vector.new(7, 10, 31)
lzr_globals.MENU_PLAYER_SPAWN_POS = vector.add(lzr_globals.MENU_SHIP_POS, lzr_globals.MENU_SHIP_PLAYER_SPAWN_OFFSET)
lzr_globals.WATER_LEVEL = 0
lzr_globals.SEABED_LEVEL = -1000
lzr_globals.SEASTONE_LEVEL = -1002
lzr_globals.GRAVITY = tonumber(minetest.settings:get("movement_gravity")) or 9.81
lzr_globals.LASER_GLOW = 3

lzr_globals.OPAQUE_LASERS = minetest.settings:get_bool("lzr_opaque_lasers", false)
