lzr_gamestate = {}

-- List of game states
lzr_gamestate.MENU = 1			-- When in the main menu
lzr_gamestate.LEVEL = 2			-- When playing a level
lzr_gamestate.LEVEL_COMPLETE = 3	-- When a level was won and waiting for the next level
lzr_gamestate.EDITOR = 4		-- TODO: When making a level in the level editor

-- Always start at the MENU state
local current_state = lzr_gamestate.MENU

-- Registered callbacks
lzr_gamestate.registered_on_enter_states = {}
lzr_gamestate.registered_on_exit_states = {}

------------
-- API calls
------------

lzr_gamestate.set_state = function(new_state)
	local state_changed = current_state ~= new_state
	local old_state = current_state

	if state_changed then
		for _, callback in pairs(lzr_gamestate.registered_on_enter_states) do
			callback(new_state)
		end
		for _, callback in pairs(lzr_gamestate.registered_on_exit_states) do
			callback(old_state)
		end
	end

	current_state = new_state
	minetest.log("action", "[lzr_gamestate] Game state changed to "..tostring(current_state))
end

lzr_gamestate.get_state = function()
	return current_state
end

lzr_gamestate.register_on_enter_state = function(callback)
	table.insert(lzr_gamestate.registered_on_enter_states, callback)
end

lzr_gamestate.register_on_exit_state = function(callback)
	table.insert(lzr_gamestate.registered_on_exit_states, callback)
end
