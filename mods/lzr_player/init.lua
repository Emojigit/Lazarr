lzr_player = {}

lzr_player.set_play_inventory = function(player)
	local inv = player:get_inventory()
	inv:set_size("main", 3)
end
lzr_player.set_editor_inventory = function(player)
	local inv = player:get_inventory()
	inv:set_size("main", 32)
end

minetest.register_on_joinplayer(function(player)
	local inv = player:get_inventory()
	inv:set_size("craft", 0)
	player:set_sky({
		sky_color = {
			day_sky="#3d4caa",
			day_horizon="#55aaff",
		},
	})
	player:set_stars({
		star_color = "#FFFFFF80",
		count = 4000,
		scale = 0.3,
	})
	player:set_clouds({
		height = 100,
		thickness = 20,
		density = 0.3,
		color = "#FFFFFF",
	})

	lzr_player.set_play_inventory(player)
end)

-- Can't drop items
function minetest.item_drop(itemstack, dropper, pos)
	if lzr_gamestate.get_state() == lzr_gamestate.EDITOR then
		-- Destroy item in editor mode
		return ""
	else
		return itemstack
	end
end
