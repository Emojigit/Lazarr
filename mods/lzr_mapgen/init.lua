-- Create ocean

local c_water = minetest.get_content_id("lzr_core:water_source")
local c_seabed = minetest.get_content_id("lzr_core:seabed")
local c_stone = minetest.get_content_id("lzr_core:stone")

minetest.register_on_generated(function(minp, maxp)
	if minp.y <= lzr_globals.WATER_LEVEL then
		local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
		local data = vm:get_data()
		local area = VoxelArea:new({MinEdge=emin, MaxEdge=emax})

		for x = minp.x, maxp.x do
			for z = minp.z, maxp.z do
				for y = minp.y, math.min(lzr_globals.WATER_LEVEL, maxp.y) do
					local p_pos = area:index(x, y, z)
					if y <= lzr_globals.SEASTONE_LEVEL then
						data[p_pos] = c_stone
					elseif y <= lzr_globals.SEABED_LEVEL then
						data[p_pos] = c_seabed
					else
						data[p_pos] = c_water
					end
				end
			end
		end

		vm:set_data(data)
		vm:update_liquids()
		vm:calc_lighting()
		vm:write_to_map()
	end
end)


