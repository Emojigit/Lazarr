local S = minetest.get_translator("lzr_tools")

local instadig = { times = { [1] = 0, [2] = 0, [3] = 0}, uses=0, maxlevel=255}

-- Cheat tools and weapons
minetest.register_tool("lzr_tools:ultra_pickaxe", {
	description = S("Ultra Pickaxe"),
	inventory_image = "lzr_tools_ultra_pickaxe.png",
	groups = { pickaxe = 1, cheat_item = 1 },

	range = 20,
	tool_capabilities = {
		groupcaps = {
			choppy = instadig,
			cracky = instadig,
			snappy = instadig,
			crumbly = instadig,
			oddly_breakable_by_hand = instadig,
			dig_immediate = instadig,
			takable = instadig,
			breakable = instadig,
		},
	},
})

minetest.register_tool("lzr_tools:ultra_bucket", {
	description = S("Ultra Bucket"),
	inventory_image = "lzr_tools_ultra_bucket.png",
	groups = { bucket = 1, cheat_item = 1 },

	range = 20,
	liquids_pointable = true,
	tool_capabilities = {
		groupcaps = {
			liquid = instadig,
		},
	},
})
