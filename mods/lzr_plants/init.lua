-- This mod adds simple decorative plants.
-- The plants have no gameplay effect and don't funtionally interact with the puzzle.
-- For puzzle gameplay purposes, a plant behaves like air:
-- Lasers shoot through (which destroys the plant) and the player can build into it.
-- Plants are not takable by the player either.
-- Once destroyed, a plant is gone for good.

local S = minetest.get_translator("lzr_plants")

minetest.register_node("lzr_plants:island_grass", {
	description = S("Island Grass"),
	tiles = { "islands_tall_grass.png" },
	wield_image = "islands_tall_grass.png",
	inventory_image = "islands_tall_grass.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 0.5, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, attached_node = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:crab_grass", {
	description = S("Crab Grass"),
	tiles = { "lzr_plants_crab_grass.png" },
	wield_image = "lzr_plants_crab_grass.png",
	inventory_image = "lzr_plants_crab_grass.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 1/16, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, attached_node = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:seaweed", {
	description = S("Seaweed"),
	tiles = { "lzr_plants_seaweed.png" },
	wield_image = "lzr_plants_seaweed.png",
	inventory_image = "lzr_plants_seaweed.png",
	drawtype = "plantlike",
	paramtype2 = "wallmounted",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 0.5, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, attached_node = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:coral_purple", {
	description = S("Purple Coral"),
	tiles = { "lzr_plants_coral_purple.png" },
	wield_image = "lzr_plants_coral_purple.png",
	inventory_image = "lzr_plants_coral_purple.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 0.5, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, attached_node = 1 },
	drop = "",
	buildable_to = true,
})

