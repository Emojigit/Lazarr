-- Remove the "pulverize" chat command.
--[[ The player is not allowed to drop or destroy any
-- items, it might leave the puzzle in an unsolvable state. ]]
minetest.unregister_chatcommand("pulverize")
