local S = minetest.get_translator("lzr_gui")
local F = minetest.formspec_escape

lzr_gui = {}

local hud_ids = {}

lzr_gui.hide_treasure_status = function(player)
	local name = player:get_player_name()
	if hud_ids[name].treasures_img then
		player:hud_remove(hud_ids[name].treasures_img)
		hud_ids[name].treasures_img = nil
	end
	if hud_ids[name].treasures_cnt then
		player:hud_remove(hud_ids[name].treasures_cnt)
		hud_ids[name].treasures_cnt = nil
	end
end

lzr_gui.update_treasure_status = function(player, treasures, total_treasures)
	local name = player:get_player_name()
	if not hud_ids[name].treasures_img then
		local id = player:hud_add({
			hud_elem_type = "image",
			position = { x = 1, y = 1 },
			name = "treasure_img",
			text = "lzr_treasure_gold_block.png",
			alignment = { x = -1, y = -1 },
			offset = { x = -10, y = -20 },
			scale = { x = 3, y = 3 },
			size = { x = 3, y = 3 },
			z_index = 0,
		})
		hud_ids[name].treasures_img = id
	end
	local treasure_text = S("@1 / @2", treasures, total_treasures)
	local color = 0xFFFFFFFF
	if treasures >= total_treasures then
		color = 0xFF00FF00
	end
	if not hud_ids[name].treasures_cnt then
		local id = player:hud_add({
			hud_elem_type = "text",
			position = { x = 1, y = 1 },
			name = "treasure_cnt",
			text = treasure_text,
			number = color,
			alignment = { x = -1, y = -1 },
			offset = { x = -70, y = -20 },
			scale = { x = 100, y = 100 },
			size = { x = 3, y = 3 },
			z_index = 0,
		})
		hud_ids[name].treasures_cnt = id
	else
		player:hud_change(hud_ids[name].treasures_cnt, "text", treasure_text)
		player:hud_change(hud_ids[name].treasures_cnt, "number", color)
	end
end

lzr_gui.set_play_gui = function(player)
	player:hud_set_hotbar_itemcount(3)
	player:hud_set_hotbar_image("lzr_gui_hotbar_3.png")
	local name = player:get_player_name()
	if hud_ids[name].editor_mode then
		player:hud_remove(hud_ids[name].editor_mode)
		hud_ids[name].editor_mode = nil
	end
end
lzr_gui.set_editor_gui = function(player)
	player:hud_set_hotbar_itemcount(8)
	player:hud_set_hotbar_image("lzr_gui_hotbar_8.png")
	local id = player:hud_add({
		hud_elem_type = "text",
		position = { x = 0, y = 1 },
		name = "editor_mode",
		text = S("Level Editor"),
		number = 0xFFFFFF,
		alignment = { x = 1, y = -1 },
		offset = { x = 5, y = -5 },
		scale = { x = 100, y = 100 },
		size = { x = 3, y = 3 },
		z_index = 0,
	})
	local name = player:get_player_name()
	hud_ids[name].editor_mode = id
	lzr_gui.hide_treasure_status(player)
end
lzr_gui.set_menu_gui = function(player)
	player:hud_set_hotbar_itemcount(3)
	player:hud_set_hotbar_image("lzr_gui_hotbar_3.png")
	local name = player:get_player_name()
	if hud_ids[name].editor_mode then
		player:hud_remove(hud_ids[name].editor_mode)
		hud_ids[name].editor_mode = nil
	end
	lzr_gui.hide_treasure_status(player)
end

minetest.register_on_joinplayer(function(player)
	player:hud_set_flags({minimap = false, minimap_radar = false, healthbar = false, breathbar = false})
	player:set_inventory_formspec(
		"formspec_version[4]size[11,6.4]label[0.5,0.5;"..F(S("Inventory")).."]list[current_player;main;0.5,0.9;8,4]"
	)
	player:set_formspec_prepend([=[
		listcolors[#5c443280;#a87d5d80;#3b2b2080;#b75647;#ffffff]
		tableoptions[background=#00000030;highlight=#3B6322;border=false]
		background9[0,0;5,5;lzr_gui_bg.png;true;4]
		style_type[button;bgimg=lzr_gui_button.png;bgimg_middle=4]
		style_type[button:hovered;bgimg=lzr_gui_button_hover.png]
		style_type[button:pressed;bgimg=lzr_gui_button_pressed.png]
		style_type[button,image_button,item_image_button;sound=lzr_sounds_button]]=])
	player:hud_set_hotbar_selected_image("lzr_gui_hotbar_selected.png")

	local name = player:get_player_name()
	hud_ids[name] = {}

	lzr_gui.set_play_gui(player)
end)

minetest.register_on_leaveplayer(function(player)
	local name = player:get_player_name()
	hud_ids[name] = nil
end)
