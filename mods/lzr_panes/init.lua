-- Load support for MT game translation.
local S = minetest.get_translator("lzr_panes")

local HTHICK = 1/16 -- half pane thickness

lzr_panes = {}
function lzr_panes.register_pane(name, def)
	local flatgroups = table.copy(def.groups)
	flatgroups.pane = 1
	minetest.register_node(":lzr_panes:" .. name .. "_flat", {
		description = def.description,
		drawtype = "nodebox",
		paramtype = "light",
		is_ground_content = false,
		sunlight_propagates = true,
		inventory_image = def.inventory_image,
		wield_image = def.wield_image,
		paramtype2 = "facedir",
		tiles = {
			def.textures[3],
			def.textures[3],
			def.textures[3],
			def.textures[3],
			def.textures[1],
			def.textures[1]
		},
		groups = flatgroups,
		sounds = def.sounds,
		use_texture_alpha = def.use_texture_alpha and "blend" or "clip",
		node_box = {
			type = "fixed",
			fixed = {{-1/2, -1/2, -HTHICK, 1/2, 1/2, HTHICK}},
		},
		selection_box = {
			type = "fixed",
			fixed = {{-1/2, -1/2, -HTHICK, 1/2, 1/2, HTHICK}},
		},
	})
end

lzr_panes.register_pane("bar", {
	description = S("Steel Bars"),
	textures = {"xpanes_bar.png", "", "xpanes_bar_top.png"},
	inventory_image = "xpanes_bar.png",
	wield_image = "xpanes_bar.png",
	groups = {breakable=1},
	sounds = lzr_sounds.node_sound_metal_defaults(),
})

