if not minetest.global_exists("screwdriver") then
	screwdriver = {}
end
local S = minetest.get_translator("lzr_decor")

local pixelbox = function(size, boxes)
	local fixed = {}
	for _, box in ipairs(boxes) do
		-- `unpack` has been changed to `table.unpack` in newest Lua versions.
		local x, y, z, w, h, l = unpack(box)
		fixed[#fixed + 1] = {
			(x / size) - 0.5,
			(y / size) - 0.5,
			(z / size) - 0.5,
			((x + w) / size) - 0.5,
			((y + h) / size) - 0.5,
			((z + l) / size) - 0.5
		}
	end

	return {type = "fixed", fixed = fixed}
end

local function register_pane(name, desc, def)
	lzr_panes.register_pane(name, {
		description = desc,
		drawtype = "airlike",
		paramtype = "light",
		textures = {"xdecor_"..name..".png", "", "xdecor_"..name.."_top.png"},
		inventory_image = "xdecor_" .. name .. ".png",
		wield_image = "xdecor_" .. name .. ".png",
		groups = def.groups,
		sounds = def.sounds or lzr_sounds.node_sound_defaults(),
	})
end

register_pane("rusty_bar", S("Rusty Iron Bars"), {
	sounds = lzr_sounds.node_sound_metal_defaults(),
	groups = {breakable = 1, rotatable = 3, pane = 1},
})

register_pane("wood_frame", S("Wood Frame"), {
	sounds = lzr_sounds.node_sound_wood_defaults(),
	groups = {breakable = 1, rotatable = 3, pane = 1},
})

minetest.register_node("lzr_decor:bonfire", {
	description = S("Bonfire"),
	walkable = false,
	drawtype = "plantlike",
	paramtype = "light",
	light_source = 11,
	inventory_image = "lzr_decor_bonfire.png^[verticalframe:7:0",
	wield_image = "lzr_decor_bonfire.png^[verticalframe:7:0",
	use_texture_alpha = "clip",
	selection_box = {
		type = "fixed",
		fixed = { -5/16, -0.5, -5/16, 5/16, 7/16, 5/16 },
	},
	tiles = {
		{ name = "lzr_decor_bonfire.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 2.0,
		},},
	},
	groups = {breakable = 1},
})

minetest.register_node("lzr_decor:barrel", {
	description = S("Barrel"),
	tiles = {"xdecor_barrel_top.png", "xdecor_barrel_top.png", "xdecor_barrel_sides.png"},
	paramtype2 = "facedir",
	groups = {breakable = 1, rotatable = 3},
	sounds = lzr_sounds.node_sound_wood_defaults()
})

local function register_storage(name, desc, def)
	minetest.register_node(name, {
		description = desc,
		tiles = def.tiles,
		drawtype = def.drawtype,
		paramtype = def.paramtype,
		paramtype2 = def.paramtype2,
		node_box = def.node_box,
		on_rotate = def.on_rotate,
		on_place = def.on_place,
		groups = def.groups or {breakable = 1, rotatable = 3},
		sounds = lzr_sounds.node_sound_wood_defaults(),
		use_texture_alpha = def.use_texture_alpha,
	})
end

register_storage("lzr_decor:cabinet", S("Wooden Cabinet"), {
	paramtype2 = "facedir",
	on_rotate = screwdriver.rotate_simple,
	tiles = {
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_cabinet_sides.png", "xdecor_cabinet_front.png"
	}
})

register_storage("lzr_decor:cabinet_half", S("Half Wooden Cabinet"), {
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = { -0.5, 0, -0.5, 0.5, 0.5, 0.5 },
	},
	on_rotate = screwdriver.rotate_simple,
	use_texture_alpha = "clip",
	tiles = {
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_half_cabinet_sides.png", "xdecor_half_cabinet_sides.png",
		"xdecor_half_cabinet_sides.png", "xdecor_half_cabinet_front.png"
	}
})

register_storage("lzr_decor:empty_shelf", S("Empty Shelf"), {
	paramtype2 = "facedir",
	on_rotate = screwdriver.rotate_simple,
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^xdecor_empty_shelf.png",
	}
})

register_storage("lzr_decor:bookshelf", S("Bookshelf"), {
	paramtype2 = "facedir",
	on_rotate = screwdriver.rotate_simple,
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^lzr_decor_bookshelf.png",
	},
})

register_storage("lzr_decor:vessels_shelf", S("Vessels Shelf"), {
	paramtype2 = "facedir",
	on_rotate = screwdriver.rotate_simple,
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^lzr_decor_vessels_shelf.png",
	},
})

register_storage("lzr_decor:multishelf", S("Multi Shelf"), {
	paramtype2 = "facedir",
	on_rotate = screwdriver.rotate_simple,
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^xdecor_multishelf.png",
	},
})

minetest.register_node("lzr_decor:candle", {
	description = S("Candle"),
	light_source = 12,
	drawtype = "torchlike",
	inventory_image = "xdecor_candle_inv.png",
	wield_image = "xdecor_candle_wield.png",
	paramtype2 = "wallmounted",
	walkable = false,
	groups = { breakable = 1 },
	tiles = {
		{
			name = "xdecor_candle_floor.png",
			animation = {type="vertical_frames", length = 1.5}
		},
		{
			name = "xdecor_candle_floor.png^[transformR180",
			animation = {type="vertical_frames", length = 1.5}
		},
		{
			name = "xdecor_candle_wall.png",
			animation = {type="vertical_frames", length = 1.5}
		}
	},
	selection_box = {
		type = "wallmounted",
		wall_top = {-0.25, -0.3, -0.25, 0.25, 0.5, 0.25},
		wall_bottom = {-0.25, -0.5, -0.25, 0.25, 0.1, 0.25},
		wall_side = {-0.5, -0.35, -0.15, -0.15, 0.4, 0.15}
	}
})

minetest.register_node("lzr_decor:chair", {
	description = S("Chair"),
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	tiles = {"xdecor_wood.png"},
	sounds = lzr_sounds.node_sound_wood_defaults(),
	groups = { breakable = 1, rotatable = 3 },
	on_rotate = screwdriver.rotate_simple,
	node_box = pixelbox(16, {
		{3,  0, 11,   2, 16, 2},
		{11, 0, 11,   2, 16, 2},
		{5,  9, 11.5, 6,  6, 1},
		{3,  0,  3,   2,  6, 2},
		{11, 0,  3,   2,  6, 2},
		{3,  6,  3,  10,  2, 8}
	}),
})

minetest.register_node("lzr_decor:cobweb", {
	description = S("Cobweb"),
	drawtype = "plantlike",
	paramtype = "light",
	tiles = {"xdecor_cobweb.png"},
	inventory_image = "xdecor_cobweb.png",
	walkable = false,
	selection_box = {type = "regular"},
	groups = { breakable = 1, laser_destroys = 1 },
	sounds = lzr_sounds.node_sound_grass_defaults({footstep={}}),
})

minetest.register_node("lzr_decor:lantern", {
	description = S("Lantern"),
	light_source = 13,
	drawtype = "plantlike",
	inventory_image = "xdecor_lantern_inv.png",
	wield_image = "xdecor_lantern_inv.png",
	walkable = false,
	groups = { breakable = 1 },
	tiles = {
		{
			name = "xdecor_lantern.png",
			animation = {type="vertical_frames", length = 1.5}
		}
	},
	selection_box = pixelbox(16, {{4, 0, 4, 8, 16, 8}})
})

local lightbox = {
	iron = { S("Iron Light Box"), "xdecor_iron_lightbox.png" },
	wooden2 = { S("Wooden Light Box"), "xdecor_wooden2_lightbox.png" },
	ship = { S("Ship Light Box"), "lzr_decor_ship_lightbox.png" },
}

for l, data in pairs(lightbox) do
	minetest.register_node("lzr_decor:" .. l .. "_lightbox", {
		description = data[1],
		paramtype = "light",
		sunlight_propagates = true,
		tiles = {data[2]},
		groups = { breakable = 1 },
		light_source = 13,
		sounds = lzr_sounds.node_sound_glass_defaults()
	})
end

local xdecor_potted = {
	dandelion_white = S("Potted White Dandelion"),
	dandelion_yellow = S("Potted Yellow Dandelion"),
	geranium = S("Potted Geranium"),
	rose = S("Potted Rose"),
	tulip = S("Potted Tulip"),
	viola = S("Potted Viola"),
}

for f, desc in pairs(xdecor_potted) do
	minetest.register_node("lzr_decor:potted_" .. f, {
		description = desc,
		walkable = false,
		groups = { breakable = 1, potted_flower = 1 },
		tiles = {"xdecor_" .. f .. "_pot.png"},
		inventory_image = "xdecor_" .. f .. "_pot.png",
		drawtype = "plantlike",
		paramtype = "light",
		selection_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, -0.2, 0.5 },
		},
	})
end

local function register_hard_node(name, desc, def)
	def = def or {}
	minetest.register_node(name, {
		description = desc,
		tiles = {"xdecor_" .. name .. ".png"},
		groups = def.groups or { breakable = 1},
		sounds = def.sounds or lzr_sounds.node_sound_stone_defaults()
	})
end

minetest.register_node("lzr_decor:table", {
	description = S("Table"),
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	tiles = {"xdecor_wood.png"},
	groups = { breakable = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
	node_box = pixelbox(16, {
		{0, 14, 0, 16, 2, 16}, {5.5, 0, 5.5, 5, 14, 6}
	})
})

minetest.register_node("lzr_decor:woodframed_glass", {
	description = S("Wood Framed Glass"),
	paramtype = "light",
	drawtype = "glasslike_framed",
	sunlight_propagates = true,
	tiles = {"xdecor_woodframed_glass.png", "xdecor_woodframed_glass_detail.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_glass_defaults()
})

minetest.register_node("lzr_decor:ocean_stone", {
	description = S("Ocean Stone"),
	tiles = {"xocean_stone.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_stone_block", {
	description = S("Ocean Stone Block"),
	tiles = {"lzr_decor_ocean_stone_block.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_cobble", {
	description = S("Ocean Cobblestone"),
	tiles = {"xocean_cobble.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_carved", {
	description = S("Carved Ocean Stone"),
	tiles = {"xocean_carved.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_circular", {
	description = S("Circular Ocean Stone"),
	tiles = {"xocean_circular.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_bricks", {
	description = S("Ocean Bricks"),
	tiles = {"xocean_brick.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_decor:ocean_pillar", {
	description = S("Ocean Pillar"),
	paramtype2 = "facedir",
	tiles = {
		"lzr_decor_ocean_pillar_top.png",
		"lzr_decor_ocean_pillar_top.png^[transformFY",
		"lzr_decor_ocean_pillar.png^[transformFX",
		"lzr_decor_ocean_pillar.png",
		"lzr_decor_ocean_pillar.png^[transformFX",
		"lzr_decor_ocean_pillar.png",
	},
	groups = { breakable = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_lantern", {
	description = S("Ocean Lantern"),
	light_source = minetest.LIGHT_MAX,
	tiles = {"xocean_lantern.png"},
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_glass_defaults(),
})


