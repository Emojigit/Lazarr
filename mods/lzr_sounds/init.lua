lzr_sounds = {}

function lzr_sounds.node_sound_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "", gain = 1.0}
	table.dug = table.dug or
			{name = "default_dug_node", gain = 0.25}
	table.place = table.place or
			{name = "default_place_node_hard", gain = 1.0}
	return table
end

function lzr_sounds.node_sound_stone_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_stone", gain = 0.9}
	table.dug = table.dug or
			{name = "lzr_sounds_footstep_stone", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_leaves_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_leaves", gain = 1.0}
	table.dug = table.dug or
			{name = "lzr_sounds_footstep_leaves", gain = 1.0}
	table.place = table.place or
			{name = "default_place_node", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_grass_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_leaves", gain = 1.0}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_grass", gain = 1.0}
	table.place = table.place or
			{name = "default_place_node", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_dirt_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "default_dirt_footstep", gain = 0.6}
	table.dug = table.dug or
			{name = "default_dirt_footstep", gain = 1.0}
	table.place = table.place or
			{name = "default_place_node", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

-- TODO: sand
lzr_sounds.node_sound_sand_defaults = lzr_sounds.node_sound_dirt_defaults

function lzr_sounds.node_sound_wood_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_wood", gain = 0.3}
	table.dug = table.dug or
			{name = "lzr_sounds_place_wood", gain = 1.0, pitch = 0.95 }
	table.place = table.place or
			{name = "lzr_sounds_place_wood", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_glass_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_glass", gain = 0.3}
	table.place = table.place or
			{name = "lzr_sounds_place_glass", gain = 0.3}
	table.dig = table.dig or
			{name = "lzr_sounds_footstep_glass", gain = 0.5}
	table.dug = table.dug or
			{name = "lzr_sounds_dig_glass", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_metal_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "default_metal_footstep", gain = 0.4}
	table.dig = table.dig or
			{name = "default_dig_metal", gain = 0.5}
	table.dug = table.dug or
			{name = "default_dug_metal", gain = 0.5}
	table.place = table.place or
			{name = "default_place_node_metal", gain = 0.5}
	lzr_sounds.node_sound_defaults(table)
	return table
end
