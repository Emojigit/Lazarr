local S = minetest.get_translator("lzr_laser")

minetest.register_tool("lzr_laser:emit_toggler", {
	description = S("Emitter Toggler"),
	inventory_image = "lzr_laser_emitter_on.png",
	groups = { cheat_item = 1 },
	on_use = function(itemstack, user, pointed_thing)
		if pointed_thing.type ~= "node" then
			return itemstack
		end
		local pos = pointed_thing.under
		local node = minetest.get_node(pos)
		local is_emit = minetest.get_item_group(node.name, "emitter") > 0
		if not is_emit then
			return itemstack
		end
		local def = minetest.registered_nodes[node.name]
		if def._lzr_on_toggle then
			def._lzr_on_toggle(pos, node)
		end
		return itemstack
	end,
})

