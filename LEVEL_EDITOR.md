# How to use the Level Editor
The game has a VERY crude level editor which isn't really user-friendly
right now. It works with Minetest chat commands (refer to Minetest help to
learn how they work). Remember you can use "`/help <command>`" for a
syntax reference of a command.

To start, first enter the "`/editor`" command in chat,
then choose a size with "`/editor_size`".
There is no Creative Mode inventory, so you have to give yourself the
blocks with "`/giveme`" or by using a mod.
The Ultra Pickaxe removes blocks.
When you're happy with the level, use "`/editor_save`". The level
will be saved in your world directory (so be careful to not delete
your world!).

To work properly, a level needs to:
* To be solvable (duh!)
* At least one treasure chest (locked or unlocked)
* Exactly ONE teleporter in "off" state (`lzr_teleporter:teleporter_off`)
  (The player starts here, it also acts as a simple game menu when punched)

Do not mind the wooden boundary of the level, this is a dummy that will
be replaced with the material specified in the level list, it is NOT
part of the level file. Instead, the level border nodes are specified in the
level list (see below).

To test a custom level, use "`editor_load`" and then "`editor exit`".
Note this is still quite buggy. The game doesn't really support playing
custom levels well, sorry. :(

## Adding the level to the game
If you think you made a nice level, send it to Wuzzy per e-mail at
Wuzzy@disroot.org. This game desperately needs more levels, so submissions
are appreciated. :D

If you don't want to wait for Wuzzy and add the level manually on your own 
computer, so that it appears in your level list, you must copy the level
`.mts` file to `mods/lzr_levels/schematics`,
give it a proper file name and then add a line to
`mods/lzr_levels/data/level_data.csv`.

A line follows this format:

    <file name>,<level name>,<border list>,<ambience>

where:

* `<file name>`: File name of the level `.mts` file
* `<level name>`: Name of the level as shown to the player, in English
* `<border list>`: List of border blocks, see below
* `<ambience>`: Ambient sounds. Either `ocean`, `temple` or `none`.

Border list is a list of nodes used for the level border.
It contains 1-4 node names, separated by `|`. The border nodes are, in this order:

* Wall|Window|Floor|Ceiling

Wall is mandatory, the rest is optional. If window, floor or ceiling are missing,
they will the same as wall.
